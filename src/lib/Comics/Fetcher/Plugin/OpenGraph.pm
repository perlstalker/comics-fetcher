package Comics::Fetcher::Plugin::OpenGraph;
use Moose::Role;
use MooseX::Method::Signatures;

use POSIX qw(strftime);

method opengraph (Str :$name) {
    my $url = URI->new($self->config->val($name, 'page'));

    my $req = HTTP::Request->new;
    $req->method('GET');
    $req->uri($url);

    $self->log->debug('Fetching from: '.$self->config->val($name, 'page')."\n");

    my $page = $self->ua->request($req);
    my $content = $page->content;

    my $img_src = '';
    my $img_desc = '';
    if ($page->is_success and $content) {
	if ($content =~ m|<meta\s+property="og:image"\s+content="(\s+)"/>|ix) {
	    $img_src = $1;
	}
	if ($content =~ m|<meta\s+property="og:description"\s+content="(\s+)"/>|ix) {
	    $img_desc = $1;
	}
    }

    $self->log->debug("OpenGraph $name img_src: '$img_src'\n");
    $self->log->debug("OpenGraph $name img_desc: '$img_desc'\n");

    my $image_file = $self->_gen_output_file(
	name => $name,
	ext  => $self->config->val($name, 'img_ext') || 'jpg',
	);

    $self->_fetch_image(
	image_url   => $img_src,
	referer     => $self->config->val($name, 'page'),
	output_file => $image_file,
	);

    if ($img_desc) {
	my $title_file = $self->_gen_output_file(
	    name => $name,
	    ext  => 'title'
	    );
	open (my $file, '>',
	      $self->config->val('_config', 'image_dir').$title_file)
	    or die "Unable to open $title_file: $!\n";
	print $file $img_desc;
	close $file;
    }
    
    return { url => $img_src, output_file => $image_file };

}

1;
