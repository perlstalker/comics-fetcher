FROM perl:5.28

ENV FETCHER_BASE_URI=/comics

RUN mkdir /comics

RUN cpanm \
 Module::Build

COPY src/ /usr/local/src/fetcher
WORKDIR /usr/local/src/fetcher

## run it twice to fix occasional build problem
RUN perl ./Build.PL && ./Build installdeps && ./Build install; \
    perl ./Build.PL && ./Build installdeps && ./Build install;

# copy in fetcher config
COPY fetcher.conf /usr/local/etc/

ENTRYPOINT /usr/local/bin/fetcher

