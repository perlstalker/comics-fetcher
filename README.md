To run:

    docker run  --user 999 \
	    -e FETCHER_BASE_URI=https://yoursite.com/fetcher \
		-e FETCHER_IMAGE_PATH=/fetcher/images \
        -v /var/www/html/comics:/comics \
	    registry.gitlab.com/perlstalker/comics-fetcher:latest

